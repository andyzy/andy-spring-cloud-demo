package org.andy.consul.client;



import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
	//	SpringApplication.run(DemoApplication.class, args);
		
        new SpringApplicationBuilder(Application.class)
        .web(true).run(args);
	}
}
