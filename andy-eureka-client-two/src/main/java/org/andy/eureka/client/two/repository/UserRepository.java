package org.andy.eureka.client.two.repository;

import org.andy.eureka.client.two.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User,Long > {

}
