package org.andy.eureka.client.two.controller;

import org.andy.eureka.client.two.entity.User;
import org.andy.eureka.client.two.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZyController {
	
	@Autowired
	DiscoveryClient discoveryClient;
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/zy")
	public String zy() throws InterruptedException{
		//Thread.sleep(5000);
		String services = "Services :"+discoveryClient.getServices();
		System.out.println(services);
		return services;
	}
	
	
	@GetMapping("/{id}")
	public User findById(@PathVariable Long id) {
		System.out.println("9912");
	User findOne = this.userRepository.findOne(id);
	return findOne;
	}
	
	
	@GetMapping("/instance-info")
	public ServiceInstance showInfo() {
	ServiceInstance localServiceInstance = this.discoveryClient.
	getLocalServiceInstance();
	return localServiceInstance;
	}

}
