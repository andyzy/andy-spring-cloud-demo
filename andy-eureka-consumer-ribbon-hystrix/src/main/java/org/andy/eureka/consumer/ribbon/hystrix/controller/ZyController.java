package org.andy.eureka.consumer.ribbon.hystrix.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ZyController {
	
	@Autowired
	ConsumerService consumerService;
	
     @GetMapping("/consumer")
	public String zy(){
    	 return consumerService.consumer();
	}
	
	
}
