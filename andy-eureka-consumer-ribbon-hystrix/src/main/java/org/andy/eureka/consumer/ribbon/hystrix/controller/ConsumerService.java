package org.andy.eureka.consumer.ribbon.hystrix.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


@Service
public class ConsumerService {
	
	@Autowired
	RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod="fallback")
	public String consumer(){
		return restTemplate.getForObject("http://andy-eureka-client/1",String.class);
	}
	
	public String fallback(){
	return "fallback";	
	}
	

}
