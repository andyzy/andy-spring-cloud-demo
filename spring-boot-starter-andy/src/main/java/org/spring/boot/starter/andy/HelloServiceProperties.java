package org.spring.boot.starter.andy;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix="andy")
public class HelloServiceProperties {
	
	private static final String MSG="world";
	
	private String msg=MSG;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	
	

}
