package org.spring.boot.starter.andy;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;



@SpringBootApplication

public class Application {

	public static void main(String[] args) {
	//	SpringApplication.run(DemoApplication.class, args);
		/*
        new SpringApplicationBuilder(Application.class)
        .web(true).run(args);*/
        SpringApplication.run(Application.class, args);
	}
}
