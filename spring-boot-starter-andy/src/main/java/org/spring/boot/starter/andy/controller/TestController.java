package org.spring.boot.starter.andy.controller;

import org.spring.boot.starter.andy.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@Autowired
	HelloService  helloService;
	
	@RequestMapping("/hello")
	public String hello(){
		
		return helloService.sayhello();
	}

}
