package org.spring.boot.starter.andy;

public class HelloService {

	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String sayhello(){
		return "hello "+msg;
	}
	
}
