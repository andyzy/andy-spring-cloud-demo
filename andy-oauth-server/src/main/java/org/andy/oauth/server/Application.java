package org.andy.oauth.server;



import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@EnableAuthorizationServer
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
        .web(true).run(args);
	}
	
}
