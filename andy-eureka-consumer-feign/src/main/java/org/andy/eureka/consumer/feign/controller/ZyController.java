package org.andy.eureka.consumer.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZyController {
	
	@Autowired
	ZyClient zyClient;
	
	
	@GetMapping("/consumer")
	public String zy(){
		return zyClient.consumer();
	}
	
	 

}
