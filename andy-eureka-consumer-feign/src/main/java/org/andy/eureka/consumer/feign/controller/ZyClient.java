package org.andy.eureka.consumer.feign.controller;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("andy-eureka-client")
public interface ZyClient {
	
	
	@GetMapping("/zy")
	String consumer();

	
}
