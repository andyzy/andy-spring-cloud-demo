package org.andy.eureka.client.config;

import org.andy.core.filter.logFilter;
import org.andy.core.filter.laFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {
	

	@Bean
	logFilter rquestLogginFilter() {
		logFilter f = new logFilter();
		f.setLogEnabled(true);
		f.setIncludeHeaders(true);
		f.setIncludeQueryString(true);
		f.setIncludePayload(true);
		f.setIncludeResponse(true);
		f.setMaxPayloadLength(1000);
		return f;
	}
	/*
	@Bean
	laFilter  rquestlaFilter(){
		laFilter l= new laFilter();
		return l;
	}*/

	
}
