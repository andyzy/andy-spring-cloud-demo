package org.andy.eureka.client.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6493222368737911508L;

	   @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private Long id;
	  @Column(name="username",length = 5)
	  private String userName;
	  @Column(name="age")
	  private Integer age;



	  public Long getId() {

	    return this.id;

	  }



	  public User(String userName, Integer age) {
		super();
		this.userName = userName;
		this.age = age;
	}



	public void setId(Long id) {

	    this.id = id;

	  }




	  public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public Integer getAge() {

	    return this.age;

	  }



	  public void setAge(Integer age) {

	    this.age = age;

	  }



	public User() {
		super();
	}



}
