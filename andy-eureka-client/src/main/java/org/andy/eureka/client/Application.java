package org.andy.eureka.client;









import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@EnableDiscoveryClient
@SpringBootApplication
@EnableCaching
//@EnableTransactionManagement //自动开启事务 不需要注解
public class Application  {

	
	
	
	public static void main(String[] args) {
	//SpringApplication.run(Application.class, args);
		
        new SpringApplicationBuilder(Application.class)
        .web(true).run(args);
	}
	
	
	@Bean
	public Object testBean(PlatformTransactionManager platformTransactionManager){
	System.out.println(">>>>>>>>>>>>>>>>>>>"+platformTransactionManager.getClass().getName());
	return null;
	}
	
}
