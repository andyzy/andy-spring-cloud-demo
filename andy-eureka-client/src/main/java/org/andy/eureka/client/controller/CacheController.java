package org.andy.eureka.client.controller;

import org.andy.eureka.client.entity.User;
import org.andy.eureka.client.service.ZyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cache")
public class CacheController {
	
	
	@Autowired
	ZyService zyService;

	
	
	@GetMapping("/find/{id}")
	public User fingById(@PathVariable Long id){
		return this.zyService.findById(id);
	}
	
	@GetMapping("/delete/{id}")
	public void deleteById(@PathVariable Long id){
		this.zyService.updateById(id);
		
	}
	@GetMapping("/save")
	public void save(){
		this.zyService.save(new User("cache",1));
	}
	
	@GetMapping("/oforValue")
	public void  oforValue(){
		this.zyService.oforValue();
	}
	
	@GetMapping("/oforObject/{id}")
	public void  oforObject(@PathVariable Long id){
		this.zyService.oforObject(id);
	}
	
	
}
