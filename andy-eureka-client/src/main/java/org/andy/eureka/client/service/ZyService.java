package org.andy.eureka.client.service;

import org.andy.eureka.client.entity.User;
import org.andy.eureka.client.repository.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class ZyService {

	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StringRedisTemplate stringRedisTemplate;
	
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	
	
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@CachePut(value="user",key="#user.id")
	public void save(User user){
		this.userRepository.save(user);
		log.info("save 缓存");
		
	}
	
	
	@Cacheable(value="user",key="#id")
	public User findById(Long id){
		User user = this.userRepository.findById(id);
		log.info("find 缓存");
		return user;
	}
	
	
	@CacheEvict(value="user",key="#id")
	public  void updateById(Long id){
		User user =new User();
		user.setId(id);
		user.setAge(1);
		user.setUserName("cache");
		this.userRepository.save(user);
		log.info("delete 缓存");
	}
	
	
	
	public void  oforValue(){
		stringRedisTemplate.opsForValue().set("zy","zy");
		
	}
	
	public void  oforObject(Long id){
		
		redisTemplate.opsForValue().set("zyUser"+id,this.userRepository.findById(id));
		redisTemplate.opsForValue().set("test"+id,"dasfddddd");
	}
}
