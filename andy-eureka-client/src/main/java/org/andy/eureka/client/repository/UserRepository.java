package org.andy.eureka.client.repository;


import java.util.List;

import org.andy.eureka.client.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;



public interface UserRepository extends JpaRepository<User,Long >,JpaSpecificationExecutor<User> {
	
	
	
	Page<User> findByUserName(String userName,Pageable pageable);
	
	User findById(Long id);
	
/*	User updateById(Long id);*/

}
