package org.andy.eureka.client.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	
	@Bean
	public JedisPoolConfig getRedisConfig(){  
	        JedisPoolConfig config = new JedisPoolConfig();  
	        return config;  
	    }  
	
	@Bean
    JedisConnectionFactory getConnectionFactory() {
	    JedisConnectionFactory factory = new JedisConnectionFactory();
	    JedisPoolConfig config = getRedisConfig();  
        factory.setPoolConfig(config);  
        log.info("-------------------------------------------------JedisConnectionFactory bean init success.");  
	    return factory;
	}
	
	
	/* @Bean  
	    public RedisTemplate<?, ?> getRedisTemplate(){  
	        RedisTemplate<?,?> template = new StringRedisTemplate(getConnectionFactory());  
	        return template;  
	    }  */
	 
	 @Bean
	 public RedisTemplate<String, Object> getRedisTemplate()
	 {
	        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
	        ObjectMapper om = new ObjectMapper();
	        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
	        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
	        jackson2JsonRedisSerializer.setObjectMapper(om);
	        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
	        template.setConnectionFactory(getConnectionFactory());
	        template.setKeySerializer(jackson2JsonRedisSerializer);
	        template.setValueSerializer(jackson2JsonRedisSerializer);
	        template.setHashKeySerializer(jackson2JsonRedisSerializer);
	        template.setHashValueSerializer(jackson2JsonRedisSerializer);
	        template.afterPropertiesSet();
	        return template;
	    }

    @Bean
    public StringRedisTemplate getStringRedisTemplate(){
    	StringRedisTemplate template=new StringRedisTemplate(getConnectionFactory());
    	RedisSerializer<String> stringSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringSerializer );
        template.setValueSerializer(stringSerializer );
        template.setHashKeySerializer(stringSerializer );
        template.setHashValueSerializer(stringSerializer );
        return template;

    }

	
	
	
	
	
}
