package org.andy.eureka.client.controller;




import org.andy.eureka.client.entity.User;
import org.andy.eureka.client.repository.RoleRepository;
import org.andy.eureka.client.repository.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.spring.boot.starter.andy.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZyController {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	DiscoveryClient discoveryClient;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
    RoleRepository roleRepository;
	
	@Autowired
	ZyController zyController;
	
	@Autowired
	HelloService helloService;
	
	@GetMapping("/zy")
	public String zy() throws InterruptedException{
		//Thread.sleep(5000);
		String services = "Services :"+discoveryClient.getServices();
		System.out.println(services);
		return services;
	}
	
	@GetMapping("/hello")
	public String hello(){
		return helloService.sayhello();
	}
	
	
	@GetMapping("/{id}")
	public User findById(@PathVariable Long id) {
    log.info(id);
	System.out.println("9911");
	User findOne = this.userRepository.findOne(id);
	return findOne;
	}
	
	
	@GetMapping("/instance-info")
	public ServiceInstance showInfo() {
	ServiceInstance localServiceInstance = this.discoveryClient.
	getLocalServiceInstance();
	return localServiceInstance;
	}
	
	@GetMapping("/page")
	public Object fingById(String userName,Integer pageSize ,Integer pageNo){
		
		Pageable page= new PageRequest(pageNo,pageSize);
		return userRepository.findByUserName(userName, page);
		
	}
	@GetMapping("/save")
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void save(){
	this.userRepository.save(new User("zy",1));
	this.userRepository.save(new User("zy1",1));
	this.userRepository.save(new User("zy2",1));
	this.userRepository.save(new User("zy3",1));
	this.userRepository.save(new User("zy4",1));
	zyController.save2();
    	
	}
	public void save2(){
		this.userRepository.save(new User("zydsadfs4",1));
	}
}
