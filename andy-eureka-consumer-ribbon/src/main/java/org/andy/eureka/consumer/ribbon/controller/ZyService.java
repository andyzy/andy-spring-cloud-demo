package org.andy.eureka.consumer.ribbon.controller;

import org.andy.eureka.consumer.ribbon.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ZyService {

	@Autowired
	RestTemplate restTemplate;
	
	public User fingById(Long id){
		 return restTemplate.getForObject("http://andy-eureka-client/"+id,User.class);
	}
}
