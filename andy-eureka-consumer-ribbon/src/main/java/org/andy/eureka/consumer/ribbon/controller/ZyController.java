package org.andy.eureka.consumer.ribbon.controller;

import org.andy.eureka.consumer.ribbon.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ZyController {
	
/*	@Autowired
	LoadBalancerClient loadBalancerClient;*/
	
/*	@Autowired
	RestTemplate restTemplate;*/
	
	@Autowired
	ZyService zyService;
	
	@GetMapping("/consumer/{id}")
	public User  zy(@PathVariable Long id){
		/* ServiceInstance s =loadBalancerClient.choose("andy-eureka-client");
		 String url="http://"+s.getHost()+":"+s.getPort()+"/zy";
		 System.out.println(url);*/
		/* return restTemplate.getForObject("http://andy-eureka-client/zy",String.class);*/
		return zyService.fingById(id);
		
	}
	
	
	

}
